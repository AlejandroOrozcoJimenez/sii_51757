//51757	
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<fcntl.h>
#include<unistd.h>

int main (int argc, char *argv[])
{
	int lectura;
	int fifo=mkfifo("/tmp/loggerfifo", 0777);
	if(fifo==-1){perror("Error al crear FIFO");}

	int ficherolog=open ("/tmp/loggerfifo", O_RDONLY);
	if (ficherolog==-1){perror("Error de aperturta FIFO");}

	char buf[500];
	while (read(ficherolog, buf, sizeof(buf)))
	{
		printf("%s\n", buf);
		
		if (lectura==-1||buf[0]=='E'){
			printf("Tenis no abierto, Cerrar Logger\n");
			break;
		}
	}
	
	int clog=close(ficherolog);
	if (clog==-1){ perror("Error al cerrar FIFO");}
	
	unlink ("/tmp/loggerfifo");
	return 0;
}
